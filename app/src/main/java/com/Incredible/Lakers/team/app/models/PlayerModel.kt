package com.Incredible.Lakers.team.app.models

data class PlayerModel(
    val imagePlayer : Int,
    val statsPlayer : Int,
    val descPlayer : Int
)
