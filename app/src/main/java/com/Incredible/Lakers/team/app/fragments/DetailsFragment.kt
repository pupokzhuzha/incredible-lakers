package com.Incredible.Lakers.team.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.Incredible.Lakers.team.app.databinding.FragmentDetailsBinding
import com.Incredible.Lakers.team.app.databinding.FragmentMainBinding
import com.Incredible.Lakers.team.app.models.PlayerModel
import com.Incredible.Lakers.team.app.util.GeneratorPlayers

class DetailsFragment : Fragment() {

    private var binding: FragmentDetailsBinding? = null
    private val mBinding get() = binding!!

    private var counterPlayers = 1
    private lateinit var actualModel : PlayerModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            initPlayer(counterPlayers)
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            next.setOnClickListener {
                if(counterPlayers < 14){
                    counterPlayers++
                    initPlayer(counterPlayers)
                    if(counterPlayers == 14){
                        next.isEnabled = false
                        enabled.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun initPlayer(counter : Int){
        mBinding.apply {
            actualModel = GeneratorPlayers.generate(counter)
            imagePlayer.setImageResource(actualModel.imagePlayer)
            statsPlayer.setImageResource(actualModel.statsPlayer)
            descPlayer.setImageResource(actualModel.descPlayer)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}