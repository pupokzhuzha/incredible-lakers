package com.Incredible.Lakers.team.app.util

import com.Incredible.Lakers.team.app.R
import com.Incredible.Lakers.team.app.models.PlayerModel

object GeneratorPlayers {

    fun generate(countPlayer : Int) : PlayerModel{
        return when(countPlayer){
            1 -> PlayerModel(R.drawable.colin, R.drawable.colin_stats, R.drawable.colin_desc)
            2 -> PlayerModel(R.drawable.max, R.drawable.max_stats, R.drawable.max_desc)
            3 -> PlayerModel(R.drawable.anthony, R.drawable.anthony_stats, R.drawable.anthony_desc)
            4 -> PlayerModel(R.drawable.alex, R.drawable.alex_stats, R.drawable.alex_desc)
            5 -> PlayerModel(R.drawable.ruiha, R.drawable.ruiha_stats, R.drawable.ruiha_desc)
            6 -> PlayerModel(R.drawable.jaxson, R.drawable.jaxson_stats, R.drawable.jaxson_desc)
            7 -> PlayerModel(R.drawable.dmoi, R.drawable.dmoi_stats, R.drawable.dmoi_desc)
            8 -> PlayerModel(R.drawable.lebron, R.drawable.lebron_stats, R.drawable.lebron_desc)
            9 -> PlayerModel(R.drawable.maxwell, R.drawable.maxwell_stats, R.drawable.maxwell_desc)
            10 -> PlayerModel(R.drawable.skylar, R.drawable.skylar_stats, R.drawable.skylar_desc)
            11 -> PlayerModel(R.drawable.taurean, R.drawable.taurean_stats, R.drawable.taurean_desc)
            12 -> PlayerModel(R.drawable.austin, R.drawable.austin_stats, R.drawable.austin_desc)
            13 -> PlayerModel(R.drawable.cam, R.drawable.cam_stats, R.drawable.cam_desc)
            else -> PlayerModel(R.drawable.jarred, R.drawable.jarred_stats, R.drawable.jarred_desc)
        }
    }
}